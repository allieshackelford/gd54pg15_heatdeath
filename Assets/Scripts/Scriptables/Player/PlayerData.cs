using UnityEngine;

[CreateAssetMenu(menuName = "PlayerData")]
public class PlayerData : ScriptableObject
{ 
    public int DefaultLayer;
    public Colors Color;
}