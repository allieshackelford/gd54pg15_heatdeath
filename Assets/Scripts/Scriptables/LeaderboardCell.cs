﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

public struct Stats {
    public int Kills;
    public int Deaths;
    public int SelfKills;
}

[CreateAssetMenu(menuName = "LeaderboardCell")]
public class LeaderboardCell : ScriptableObject
{
    public Colors Color;
    public Stats TotalStats;
    public Stats CurrentLevelStats;
    public Color _WinColor;
    public Color _LoseColor;

    public Sprite _PlayerSprite;

    public void Awake()
    {
        TotalStats = new Stats();
        CurrentLevelStats = new Stats();
    }

    public void AddKill()
    {
        CurrentLevelStats.Kills++;
    }

    public void AddDeath()
    {
        CurrentLevelStats.Deaths++;
    }

    public void AddSelf()
    {
        CurrentLevelStats.SelfKills++;
    }

    public void AddAll()
    {
        TotalStats.Kills += CurrentLevelStats.Kills;
        TotalStats.Deaths += CurrentLevelStats.Deaths;
        TotalStats.SelfKills += CurrentLevelStats.SelfKills;
    }

    public void Reset(){
        CurrentLevelStats.Kills = 0;
        CurrentLevelStats.Deaths = 0;
        CurrentLevelStats.SelfKills = 0;
    }
}
