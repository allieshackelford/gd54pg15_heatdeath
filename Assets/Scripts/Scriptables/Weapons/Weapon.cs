﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

public abstract class Weapon : ScriptableObject
{ 
    public Sprite[] gunColorSprites; // blue, green, orange, pink
    public Sprite plainSprite;
    public float PushBack;
    public float CoolDown;
    public float AltCoolDown;
    public abstract void Shoot(Transform gun, Bullet  bullet, GunController gunController);
}
