﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine.SceneManagement;
using UnityEngine;

public class MoveCredits : MonoBehaviour
{
    //Delares Variables
    [SerializeField] private GameObject _Credits = null;
    [SerializeField] private float _ScrollSpeed = 0.0f;
    private float _Move;

    
    void Update()
    {
        //Automatic Exit
        if (_Credits.transform.position.y >= 18)
        {
            BackToMenu();
        }
    }

    private void OnInteract() {
        BackToMenu();
    }

    //Moves Credits Upwards
    private void FixedUpdate()
    {
        _Move += _ScrollSpeed;
        transform.Translate(0,_ScrollSpeed, 0);
    }

    //Quits to Menu
    private void BackToMenu()
    {
        SceneManager.LoadScene("Start");
    }
}
