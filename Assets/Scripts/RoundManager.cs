﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System;
using UnityEngine;

public class RoundManager : Singleton<RoundManager>
{
    // to monitor in inspector
    [SerializeField] private int _Rounds = 2;

    protected override void SingletonAwake(){
        DontDestroyOnLoad(gameObject);
    }

    public void SetRounds(int rounds)
    {
        _Rounds = rounds;
    }

    public void NextRound()
    {
        _Rounds--;
        Time.timeScale = 1f;
        if(_Rounds <= 0)
        {
            GameSceneManager.Instance.LoadEndScene();
            GameState.Instance.StateChange(EGameState.EndGame);
            return;
        }
        GameSceneManager.Instance.NextScene();
        GameState.Instance.StateChange(EGameState.StartLevel);
    }

   
}
