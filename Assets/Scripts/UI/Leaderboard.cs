﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System;
using UnityEngine;

public class Leaderboard : Singleton<Leaderboard>
{
    [SerializeField] public LeaderboardCell[] _Players = null;

    protected override void SingletonAwake()
    {
        DontDestroyOnLoad(gameObject);
    }  

    public void ResetLevel()
    {
        ResetScores();
        Time.timeScale = 1f;
    }

    private void ResetScores(){
        foreach(var item in _Players){
            item.Reset();
        }
    }

    private void TotalScores() {
        foreach(var item in _Players){
            item.AddAll();
        }
    }

    public LeaderboardCell[] UpdateLeaderboard()
    {
        TotalScores();
        LeaderboardCell[] _temp = new LeaderboardCell[4];
        Array.Copy(_Players, _temp, 4);
        // sort players score 
        for(int i = 0; i < _temp.Length; i++){
            var cell = _temp[i];
            int j = i - 1;

            while(j >= 0 && _temp[j].TotalStats.Kills < cell.TotalStats.Kills){
                _temp[j+1] = _temp[j];
                j--;
            }
            _temp[j+1] = cell;
        }

        return _temp;
    }

     public LeaderboardCell ReportWinner(){
        LeaderboardCell[] _temp = new LeaderboardCell[4];
        Array.Copy(_Players, _temp, 4);
        // sort players score 
        for(int i = 0; i < _temp.Length; i++){
            var cell = _temp[i];
            int j = i - 1;

            while(j >= 0 && _temp[j].TotalStats.Kills < cell.TotalStats.Kills){
                _temp[j+1] = _temp[j];
                j--;
            }
            _temp[j+1] = cell;
        }

        return _temp[0];
    }

}
