﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Canvas _PauseCanvas = null;
    [SerializeField] private Canvas _UIScores = null;
    [SerializeField] private TextMeshProUGUI _RoundNum = null;
    private int _SoundController = 0; // 0 means sound on, 1 means sound is closed
    private int _Randomer;
    public int _Rounds = 2;

// ========= MAIN MENU ==========================
    public void OnStart(){
        RoundManager.Instance.SetRounds(_Rounds);
        GameSceneManager.Instance.LoadingScene();
        GameState.Instance.StateChange(EGameState.StartLevel);
    }

    public void OnExit()
    {
        Application.Quit();
    }
// =============OPTIONS MENU=====================
    public void OnSound()
    {
        if (_SoundController > 1)
        {
            _SoundController = 0;
        }

        else {
            _SoundController++;
        }
    }

    public void OnCredits()
    {
        SceneManager.LoadScene("Credits");
    }

// =========PAUSE MENU===================================

     public void OnContinue(){
        _PauseCanvas.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    public void OnBackMenu(){
        Time.timeScale = 1f;
        SceneManager.LoadScene("Start");
    }

    private void OnPause()//Pause game
    {
        if(_PauseCanvas != null){
            _PauseCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    // ========ROUNDS OPTION=============
    private void Start()
    {
        if(_RoundNum != null)
        {
            _RoundNum.text = _Rounds.ToString();
        }
    }

    public void OnMoreRounds()
    {
        _Rounds++;
        if(_Rounds >= 7){
            _Rounds = 1;
        }
        _RoundNum.text = _Rounds.ToString();
    }


    //  =========LOADING SCENE ===================
    public void StartGame(){
        GameSceneManager.Instance.NextScene();
    }
}
