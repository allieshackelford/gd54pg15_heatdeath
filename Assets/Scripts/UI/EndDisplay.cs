﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndDisplay : MonoBehaviour
{
    [SerializeField] private Sprite[] _Images = null;
    [SerializeField] private Image _DisplayImage = null;
    [SerializeField] private SpawnedHeads[] _Heads = null;

     private void Awake() {
        LeaderboardCell cell = Leaderboard.Instance.ReportWinner();
        _DisplayImage.sprite = _Images[(int)cell.Color];
        HeadSpawner.Instance.AssignHead(_Heads[(int)cell.Color]);
    }

    private void OnInteract() {
        SceneManager.LoadScene("Start");
    }
}
