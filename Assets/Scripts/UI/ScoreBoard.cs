﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    
    [SerializeField] private Cell[] _UIElements;
    [SerializeField] private GameObject _Canvas;
    // Start is called before the first frame update]

    private void OnEnable()
    {
        GameState.Instance.StateDelegate += CheckEndState;
    }

    private void OnDisable()
    {
        GameState.Instance.StateDelegate -= CheckEndState;
    }

    void Start()
    {
        _Canvas.SetActive(false);
    }

    private void OnInteract() {
        // reset cells
        if ( _Canvas.activeInHierarchy) {
            _Canvas.SetActive(false);
            Leaderboard.Instance.ResetLevel();
            RoundManager.Instance.NextRound();
        }
    }

    private void CheckEndState(EGameState state) {
        if(state == EGameState.EndLevel) {
            _Canvas.SetActive(true);
            SetScores(Leaderboard.Instance.UpdateLeaderboard());
            Time.timeScale = 0;
        }
    }

    private void SetScores(LeaderboardCell[] cells) {
        // iterate cells and setelements
        for(int i = 0; i < _UIElements.Length; i++){
            if(i == 0){
                _UIElements[i].SetWinner(cells[i]);
            }
            else{
                _UIElements[i].SetLoser(cells[i]);
            }
        }
    }
}
