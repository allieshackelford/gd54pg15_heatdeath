﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using TMPro;

public class ShowScore : MonoBehaviour
{
    public Colors Color;
    [SerializeField] private TextMeshProUGUI _Score = null;

    private void Start() { 
        _Score.text = Leaderboard.Instance._Players[(int)Color].CurrentLevelStats.Kills.ToString();
    }

    private void Update() {
        _Score.text = Leaderboard.Instance._Players[(int)Color].CurrentLevelStats.Kills.ToString();
    }
}
