﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDVisibility : MonoBehaviour
{
    [SerializeField] private Canvas _HUD = null;
    private void OnEnable()
    {
        GameState.Instance.StateDelegate += StateChange;
    }

    private void OnDisable()
    {
        GameState.Instance.StateDelegate -= StateChange;
    }

    private void StateChange(EGameState state) {
        if (state == EGameState.EndLevel) {
            _HUD.gameObject.SetActive(false);
        }
        if (state == EGameState.StartLevel) {
            _HUD.gameObject.SetActive(true);
        }
    }
}
