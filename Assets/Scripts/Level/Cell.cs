﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Cell : MonoBehaviour
{
    [SerializeField] private Image _Cell = null;
    [SerializeField] private Image _PlayerImage = null;
    [SerializeField] private TextMeshProUGUI _Kills = null;
    [SerializeField] private TextMeshProUGUI _Deaths = null;
    [SerializeField] private TextMeshProUGUI _Self = null;

    private void Start() {
        _Cell = GetComponent<Image>();
    }

    private void SetElements(LeaderboardCell cell){
        _PlayerImage.sprite = cell._PlayerSprite;
        _Kills.text = "KILLS: " + cell.TotalStats.Kills.ToString();
        _Deaths.text = "DEATHS: " + cell.TotalStats.Deaths.ToString();
        _Self.text = "SELF: " + cell.TotalStats.SelfKills.ToString();
    }
    
    public void SetWinner(LeaderboardCell cell){
        SetElements(cell);
        _Cell.color = cell._WinColor;
    }

    public void SetLoser(LeaderboardCell cell){
        SetElements(cell);
        _Cell.color = cell._LoseColor;
    }
}
