﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileSwitch : MonoBehaviour
{
    private Tilemap _TileMap;
    private Tile _TileChange;
    public int TileIndex;

    // bool to activate/deactivate adding new color tiles when walking
    private void Start() {
        _TileMap = FindObjectOfType<Tilemap>();
        _TileChange = FindObjectOfType<TileChoice>().PlayerTile[TileIndex];
    }

    private void Update()
    {
        if(_TileMap.GetTile(Vector3Int.FloorToInt(transform.position)) != null){
            _TileMap.SetTile(Vector3Int.FloorToInt(transform.position), _TileChange);
        }
    }

}
