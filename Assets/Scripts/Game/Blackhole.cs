﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackhole : MonoBehaviour
{
    public static List<Blackhole> Blackholes = new List<Blackhole>();

    // cached values
    [SerializeField] private Blackhole _Blackhole = null;
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;
    public Vector3 Scale;  // get local scale
    private PointEffector2D _Effector; // get parent point effector
    private BlackholeAudio _Audio;
    
    // cool down and merge variables
    private bool _CanMerge = true;
    private readonly float _CoolDown = 0.8f;
    private float _NextTime;
    public Colors Color = Colors.White;
    public bool _ChosenOne;
    public bool MergeSpawn;

    // for menus
    public bool _Disabled;
    public bool _NoSound;

    private void OnEnable() {
        if (!_Disabled) GameState.Instance.StateDelegate += CheckEndState;
        if(Blackholes.Contains(this)) return;
        Blackholes.Add(this);    
    }

    private void OnDisable() {
        if (!_Disabled) GameState.Instance.StateDelegate -= CheckEndState;
        if(Blackholes.Contains(this) == false) return;
        Blackholes.Remove(this);
    }

    private void Awake()
    {
        Color = Colors.White;
        // reset local scale for lerping
        Scale = transform.localScale;
        _Effector = GetComponent<PointEffector2D>();
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Anim = GetComponent<Animator>();
        _Audio = GetComponent<BlackholeAudio>();
    }

    private void Start() {
        
        if(!MergeSpawn || !_NoSound) _Audio.PlaySpawn();
    }

    private void CheckEndState(EGameState state) {
        if(state == EGameState.EndLevel && _ChosenOne) {
            DestroySelf();
        }
    }

    private void Update()
    {
        if(_Disabled) return;
        if (GameState.State == EGameState.BlackholeGrow && !_ChosenOne) DestroySelf();
        GrowOverTime();
    }

    public void GameOverState()
    {
        FindObjectOfType<CameraShake>().Shake(.5f,2,1);
        StartCoroutine(EndGameGrow());
    }

    private IEnumerator EndGameGrow()
    {
        bool growing = true;
        GameState.Instance.StateChange(EGameState.BlackholeGrow);
        Vector3 newScale = transform.localScale * 5f;
        _Audio.PlayCritical();
        yield return new WaitForSeconds(1f);
        FindObjectOfType<CameraShake>().Shake(1.5f,4,2);
        while (growing)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, newScale, 0.005f * Time.time);
            if (transform.localScale.x >= newScale.x - .10) growing = false;
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        GameState.Instance.StateChange(EGameState.EndLevel);
    }

    private void GrowOverTime(){
        if (GameState.State != EGameState.StartLevel) return;
        // slowly increase local scale over time
        Scale += new Vector3(.0005f, .0005f, .0005f);
        transform.localScale = Scale;
        // slowly increase gravity of blackhole
        _Effector.forceMagnitude = _Effector.forceMagnitude - .05f;
    }

    public void BlackholeMerge(Collider2D collision){
        FindObjectOfType<CameraShake>().Shake(0.5f,1,1);
        FindObjectOfType<PostProc>().MergeProc();

        // if smaller, return and let larger blackhole handle merge
        Transform other = collision.gameObject.transform.parent;
        if(transform.localScale.x < other.transform.localScale.x) return;
        // checker so two blackholes aren't created when they merge
        if(_CanMerge == false) return;
        _CanMerge = false;
        collision.GetComponentInParent<Blackhole>()._CanMerge = false;
        _Audio.PlayMerge();
        // create new blackhole where two merged
        Vector3 middlePos = (transform.position + collision.transform.position) / 2;
        Blackhole hole = Instantiate(_Blackhole, middlePos, transform.rotation) as Blackhole;
        hole.MergeSpawn = true;
        Vector3 tempScale = Scale + other.transform.localScale;
        // update scale to be the size of both blackholes / 1.5 (small loss in size)
        hole.transform.localScale = new Vector3(tempScale.x / 1f, tempScale.y / 1f, tempScale.z / 1f);
        // blackhole destroys self and other
        DestroySelf();
        other.gameObject.GetComponent<Blackhole>().DestroySelf();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void BulletGrowth(Collider2D collision){
        if (Time.time > _NextTime)
        {
            // reset cooldown counter
            _NextTime = Time.time + _CoolDown;
            // increase blackhole localScale
            Scale += new Vector3(.3f, .3f, .3f);
            transform.localScale = Scale;

            // change blackhole point effector
            _Effector.forceMagnitude = _Effector.forceMagnitude - 5;
            _Rigidbody.mass += 5;
        }
        // destroy bullet
        Destroy(collision.gameObject);
    }

    public void SetAnim(Collider2D collision){
        switch(collision.gameObject.layer){
            case 8: // blue
                _Anim.SetTrigger("BlueHit");
                Color = Colors.Blue;
                break;
            case 9:    // green
                Color = Colors.Green;
                _Anim.SetTrigger("GreenHit");
                break;
            case 10:   // orange 
                Color = Colors.Orange;
                _Anim.SetTrigger("OrangeHit");
                break;
            case 11:   // pink
                Color = Colors.Pink;
                _Anim.SetTrigger("PinkHit");
                break;
        }
    }
}
