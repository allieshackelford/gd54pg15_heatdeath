﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using UnityEngine;

public class Bullet : MonoBehaviour
{
    // gameobject to be instantiated
    [SerializeField] private GameObject _Blackhole = null;
    private Rigidbody2D _Rigidbody;
    public bool IsCreated;
    public int PlayerID;
    public Colors Color;


    private void Start()
    {
        // get rigidbody and apply impulse force 
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Rigidbody.AddForce(gameObject.transform.right * 15f, ForceMode2D.Impulse);
        Destroy(gameObject, 4f);
    }

    private void Update()
    { 
        // check change in direction and rotate towards that direction
        float angle = Mathf.Atan2(_Rigidbody.velocity.y, _Rigidbody.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string hitTag = collision.gameObject.tag;
        switch(hitTag) {
            case "Bullet": {
                Instantiate(_Blackhole, transform.position, transform.rotation);
                Destroy(gameObject);
                break;
            }
            case "Player": {
                Leaderboard.Instance._Players[PlayerID].AddKill();
                Destroy(gameObject);
                break;
            }
            case "Unbreakable": {
                break;
            }
            default: break;
        }
    }
}
