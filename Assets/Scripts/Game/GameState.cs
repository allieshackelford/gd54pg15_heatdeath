// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Colors{
    Blue,
    Green,
    Orange,
    Pink,
    White 
}

public enum EGameState {
    Menu,
    Paused,
    StartLevel,
    BlackholeGrow,
    EndLevel,
    EndGame
} 

public class GameState : Singleton<GameState>
{
    public delegate void OnStateChange(EGameState state);
    public event OnStateChange StateDelegate;
    public static EGameState State;

    protected override void SingletonAwake(){
        DontDestroyOnLoad(gameObject);
        State = EGameState.Menu;
    }

    public void StateChange(EGameState newState) {
        State = newState;
        if (StateDelegate != null) {
            StateDelegate(newState);
        }
    }
}