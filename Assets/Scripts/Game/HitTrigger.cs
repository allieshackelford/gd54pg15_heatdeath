﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTrigger : MonoBehaviour
{
    private Blackhole _Blackhole;

    private void Awake()
    {
        _Blackhole = GetComponentInParent<Blackhole>();   
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Blackhole")
        {
            _Blackhole.BlackholeMerge(other);
        }
       
        else if (other.gameObject.tag == "Bullet")
        {
            if(CheckBulletColor(other)){
                _Blackhole.BulletGrowth(other);
                _Blackhole.SetAnim(other);
                return;
            }
            Destroy(other.gameObject);
        }

        else if(other.gameObject.tag == "Head"){
            _Blackhole.SetAnim(other);
            Destroy(other.gameObject);
        }
    }

    private bool CheckBulletColor(Collider2D bullet){
        if (_Blackhole.Color != bullet.gameObject.GetComponent<Bullet>().Color) {
            return true;
        }
        return false;
    }
}
