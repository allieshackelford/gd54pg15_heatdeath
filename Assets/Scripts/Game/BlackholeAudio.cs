﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackholeAudio : MonoBehaviour
{ 
    [SerializeField] private AudioClip _SpawnSound = null;
    [SerializeField] private AudioClip _MergeSound = null;
    [SerializeField] private AudioClip _CriticalMass = null;
    [SerializeField] private AudioSource _Source = null;


    public void PlaySpawn(){
        _Source.clip = _SpawnSound;
        _Source.Play();
    }

    public void PlayMerge(){
        _Source.clip = _MergeSound;
        _Source.Play();
    }

    public void PlayCritical(){
        _Source.clip = _CriticalMass;
        _Source.Play();
    }
}
