﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProc : MonoBehaviour
{
    private PostProcessVolume _Volume;
    [SerializeField] private PostProcessProfile _Original = null;
    [SerializeField] private PostProcessProfile _OtherProfile = null;
    private bool _Changing;

     private void OnEnable()
    {
        GameState.Instance.StateDelegate += PausedState;
    }

    private void OnDisable()
    {
        GameState.Instance.StateDelegate -= PausedState;
    }

    private void Start() {
        _Volume = GetComponent<PostProcessVolume>();
    }

    private void PausedState(EGameState State) {
        if(State == EGameState.Paused){
            _Volume.profile = _OtherProfile;
        }
        else{
            _Volume.profile = _Original;
        }
    }

    public void MergeProc(){
        if(_Changing) return;
        StartCoroutine(LerpChromAberr());
    }

    private IEnumerator LerpChromAberr(){
        _Changing = true;
        // change to different profile
        _Volume.profile = _OtherProfile;

        yield return new WaitForSeconds(.6f);

        _Volume.profile = _Original;
        // change back to original
        _Changing = false;
    }
}
