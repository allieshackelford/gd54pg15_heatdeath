﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GunController : MonoBehaviour
{
    [SerializeField] private Transform _ShootPoint = null;
    [SerializeField] private Bullet _Bullet = null;
    // cached gameobjects
    [SerializeField] private Weapon _DefaultGun = null;
    [SerializeField] private Weapon _AlternateGun = null;
    [SerializeField] private SpriteRenderer _Crosshair = null;
    [SerializeField] private Colors _Color = Colors.White;
    [SerializeField] private SpriteRenderer _GunRend = null;
    private PlayerAudio _Audio;
    private Animator _Anim;

    // input helpers
    private float _DeadZone = .25f;
    public bool IsGhost;
    private float _AnglePlayer;

    private float _NextShot;
    private float _NextAltShot;

    private void Start() {
        _Audio = GetComponentInParent<PlayerAudio>();
        _Anim = GetComponentInParent<Animator>();
        if(_Crosshair != null) _Crosshair.enabled = false;
        SetGunSprite(_DefaultGun);
    }

     private IEnumerator GunTimeout(){
        float gunTimer = 700.0f;
        while(gunTimer > 0) {
            gunTimer--;
            if(gunTimer <= 0) SetGunSprite(_DefaultGun);
        }

        yield return null;
    }

    private void OnAiming(InputValue value){
        var aiming = value.Get<Vector2>();
        aiming.Normalize();

        Vector2 deadZoneCheck = new Vector2(aiming.x, aiming.y);
        if(deadZoneCheck.magnitude < _DeadZone)
        {
            if(_Crosshair != null) _Crosshair.enabled = false;
            return;
        }

        _AnglePlayer = Mathf.Atan2(aiming.y, aiming.x) * Mathf.Rad2Deg;
        // flip gun sprite if player looking left
        if(_AnglePlayer < -91 || _AnglePlayer > 91) {
            if(_GunRend != null) _GunRend.flipY = true;
        }
        else {
            if(_GunRend != null) _GunRend.flipY = false;
        }

        _Anim.SetFloat("Horizontal", aiming.x);
        _Anim.SetFloat("Vertical", aiming.y);
        transform.rotation = Quaternion.Euler(0, 0, _AnglePlayer);
        if(_Crosshair != null)_Crosshair.enabled = true;
    }

    // shooting
    private void OnShoot()
    {
        //shoot if not ghost
        if (IsGhost == false && Time.time > _NextShot)
        {
            
            _Audio.PlayGun();
            // TODO: figure out how to clean this up
            if(_AlternateGun != null)
            {
                _NextShot = Time.time + _AlternateGun.CoolDown;
                _NextAltShot = Time.time + _AlternateGun.AltCoolDown;
                _AlternateGun.Shoot(_ShootPoint, _Bullet, this);
                GetComponentInParent<Rigidbody2D>().AddForce(-transform.right * _AlternateGun.PushBack, ForceMode2D.Impulse);
                return;
            }
            _NextShot = Time.time + _DefaultGun.CoolDown;
            _NextAltShot = Time.time + _DefaultGun.AltCoolDown;
            _DefaultGun.Shoot(_ShootPoint, _Bullet, this);
            GetComponentInParent<Rigidbody2D>().AddForce(-transform.right * _DefaultGun.PushBack, ForceMode2D.Impulse);
        }
    }

    public void SetGun(Weapon newGun){
        
        SetGunSprite(newGun);
        _Audio.PlayPickUp();
        _AlternateGun = newGun;
        // gun timer
        StartCoroutine(GunTimeout());
    }

    private void SetGunSprite(Weapon setGun){
        if(_GunRend == null) return;
        _GunRend.sprite = setGun.gunColorSprites[(int)_Color];
    }
}
