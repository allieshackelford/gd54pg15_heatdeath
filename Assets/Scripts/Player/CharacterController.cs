﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterController : MonoBehaviour
{
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;
    [SerializeField] private PlayerData _PlayerData = null;

    [SerializeField] private float _MoveSpeed = 4.0f;
    [SerializeField] private GameObject _GhostPlayer = null;

    public bool _Dying;

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Anim = GetComponent<Animator>();
        // if(!Leaderboard._Ended) _Audio.PlaySpawn();
    }

    private void OnMove(InputValue value) 
    {
        var direction = value.Get<Vector2>();
        _Rigidbody.velocity = new Vector2(direction.x, direction.y) * _MoveSpeed;
        _Anim.SetFloat("Velocity", _Rigidbody.velocity.magnitude);
    }

    public void Respawn()
    {
        // set layer to 'Respawn
        gameObject.layer = 18;
        StartCoroutine(InvulnerableTimer());
    }

    private IEnumerator InvulnerableTimer() {
        float invulnerable = 500;   
        while(invulnerable > 0) {
            invulnerable--;
             if(invulnerable == 0) {
                gameObject.layer = _PlayerData.DefaultLayer;
            }
        }
        yield return null;
    }

    private IEnumerator Die()
    {
        _Dying = true;
        // set layer to 'Respawn' so Bullets cannot continually register kills
        gameObject.layer = 18;
        Leaderboard.Instance._Players[(int)_PlayerData.Color].AddDeath(); 
        _Rigidbody.velocity = new Vector2(0, 0);
        // _Audio.PlayDeath();
        _Anim.SetTrigger("Dead");
        yield return new WaitForSeconds(.5f);
        _Anim.ResetTrigger("Dead");
        yield return new WaitForSeconds(.3f);
        Instantiate(_GhostPlayer, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.tag == "Bullet"){
            if (!_Dying) StartCoroutine(Die()); 
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.tag == "Blackhole"){
            if(!_Dying) StartCoroutine(Die());
        }
    }

}
