﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerGhost : MonoBehaviour
{
    private Rigidbody2D _Rigidbody;
    private Animator _Anim;

    [SerializeField] private float _MoveSpeed = 80f;
    [SerializeField] private GameObject _Player = null;
    private float _GhostTimer;

    private void Start()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Anim = GetComponentInChildren<Animator>();
        _GhostTimer = 1000f; 
    }

    private void Update()
    {
        UpdateGhost();
    }

    private void UpdateGhost() {
        _GhostTimer--;
        if(_GhostTimer <= 0)
        {
            GameObject newPlayer = Instantiate(_Player, transform.position, transform.rotation) as GameObject;
            newPlayer.GetComponent<CharacterController>().Respawn();
            Destroy(gameObject);
        }
    }

    // movement
     private void OnMove(InputValue value) 
    {
        var direction = value.Get<Vector2>();
        _Rigidbody.velocity = new Vector2(direction.x, direction.y) * _MoveSpeed;
        _Anim.SetFloat("Velocity", _Rigidbody.velocity.magnitude);
    }
}
