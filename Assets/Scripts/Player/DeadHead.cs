﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadHead : MonoBehaviour
{
    private SpriteRenderer _HeadSprite;
    private Sprite _BrokenHead;
    private BoxCollider2D _Collider;

    private float speed = 35f;
    private Vector3 _EndPos;



    private void Awake()
    {
        _Collider = GetComponent<BoxCollider2D>();
        _HeadSprite = GetComponent<SpriteRenderer>();
        _Collider.enabled = false;
        _EndPos = new Vector3(transform.position.x, transform.position.y - .40f, transform.position.z);
        Destroy(gameObject, 5);
    }

    public void SetHead(Sprite newHead, Sprite brokenHead)
    {
        _HeadSprite.sprite = newHead;
        _BrokenHead = brokenHead;
        StartCoroutine(HeadFall());
    }

    private IEnumerator HeadFall()
    {
        bool falling = true;
        while (falling)
        {
            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(transform.position, _EndPos, speed * Time.time);

            if (transform.position.y == _EndPos.y) falling = false;

            yield return null;
        }
        _HeadSprite.sprite = _BrokenHead;
        // _Collider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _HeadSprite.sprite = _BrokenHead;
        _Collider.enabled = false;
    }
}
