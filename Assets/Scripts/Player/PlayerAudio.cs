﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    [SerializeField] private AudioClip _SpawnSound = null;
    [SerializeField] private AudioClip _PickUpSound = null;
    [SerializeField] private AudioClip _DeathSound = null;
    [SerializeField] private AudioClip _GunSound = null;
    [SerializeField] private AudioSource _Source = null;


    public void PlaySpawn(){
        _Source.clip = _SpawnSound;
        _Source.Play();
    }

    public void PlayPickUp(){
        _Source.clip = _PickUpSound;
        _Source.Play();
    }

    public void PlayDeath(){
        _Source.clip = _DeathSound;
        _Source.Play();
    }

    public void PlayGun(){
        _Source.clip = _GunSound;
        _Source.Play();
    }
}
