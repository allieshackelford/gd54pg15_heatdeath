﻿// (C) Copyright 2019 Alexandrea Shackelford. All Rights Reserved. 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadSpawner : Singleton<HeadSpawner>
{
    [SerializeField] private SpawnedHeads[] _Heads = null;

    public void AssignHead(SpawnedHeads head){
        _Heads[0] = head;
    }


    private IEnumerator SpawnHeads(){
        while (true)
        {
            // spawn heads continuously, in random time intervals
            float waitTime = Random.Range(0, .8f);
            Spawn();
            yield return new WaitForSeconds(waitTime);
        }
    }

    private void Spawn()
    {
        Vector3 randomPos = new Vector3(transform.position.x + Random.Range(-20, 20),
                            transform.position.y, transform.position.z);

        int randIndex = Random.Range(0, _Heads.Length);
        float randRotate = Random.Range(0,180);

        SpawnedHeads head = Instantiate(_Heads[randIndex], randomPos, Quaternion.Euler(0,0,randRotate));
        Destroy(head.gameObject, 5);
    }

    protected override void SingletonAwake()
    {
        StartCoroutine(SpawnHeads());
    }
}
