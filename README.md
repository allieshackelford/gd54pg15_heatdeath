# All Hail the Heat Death
All Hail the Heat Death is a 4 person local multiplayer bullet hell.  Bullets that collide will form a blackhole which results in gravitational pull.  Use them to your advantage to bend your bullets or get sucked into them.

## How to Play
Open the game from the download link below.

## Controls
# Controller (Fully supported)
- Left stick to move
- Right stick to aim
- Bumpers / Triggers to shoot


# DOWNLOAD 
COMING SOON

## Screenshots

![](/Images/menu.PNG)

![](/Images/playing.PNG)

![](/Images/blackhole.PNG)

![](/Images/score.PNG)


Copyright (C) 2020, Alexandrea Shackelford. All Rights Reserved.

